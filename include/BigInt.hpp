#ifndef BIGINT_H
#define BIGINT_H

#include <cmath>
#include <string>
#include <cstdint>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <exception>
#include <functional>

class BigInt {

private:

  std::string num;
  bool negative = false;

public:

  // Constructors

  BigInt(const char* s);
  BigInt(const BigInt& b);
  BigInt(const int64_t n = 0);
  BigInt(const std::string& s) : BigInt(s.c_str()) {};

  // Arithmetic operators

  BigInt operator + (const BigInt& b);
  BigInt operator - (const BigInt& b);
  BigInt operator * (const BigInt& b);
  BigInt operator / (const BigInt& b);

  BigInt& operator += (const BigInt& b);
  BigInt& operator -= (const BigInt& b);
  BigInt& operator *= (const BigInt& b);
  BigInt& operator /= (const BigInt& b);

  friend BigInt operator + (const int64_t n, BigInt& b);
  friend BigInt operator - (const int64_t n, BigInt& b);
  friend BigInt operator * (const int64_t n, BigInt& b);
  friend BigInt operator / (const int64_t n, BigInt& b);

  // Unary operators
  BigInt operator - ();

  BigInt& operator -- ();
  BigInt& operator ++ ();

  BigInt operator ++ (int);
  BigInt operator -- (int);

  // Comparision and assignment operators

  template <typename T>
  BigInt operator = (T t) {
    return BigInt(t);
  }

  bool operator > (const BigInt& b);
  bool operator < (const BigInt& b);
  bool operator == (const BigInt& b);
  bool operator != (const BigInt& b);
  bool operator <= (const BigInt& b);
  bool operator >= (const BigInt& b);

  bool operator > (const int64_t n);
  bool operator < (const int64_t n);
  bool operator == (const int64_t n);
  bool operator != (const int64_t n);
  bool operator <= (const int64_t n);
  bool operator >= (const int64_t n);

  // IO

  friend std::istream& operator >> (std::istream& in, BigInt& b);
  friend std::ostream& operator << (std::ostream& os, const BigInt& b);
  friend std::string to_string(BigInt const& self) {
    std::string res;
    if (self.negative)
      res = "-" + self.num;
    else
      res = self.num;
    return res;
  }

  // Misc
  BigInt& turn_negative() {
    negative = true;
    return *this;
  }

  BigInt const c_turn_positive() {
    negative = false;
    return *this;
  }

  BigInt& turn_positive() {
    negative = false;
    return *this;
  }

  BigInt const abs() {
    BigInt temp(this->num);
    return temp.c_turn_positive();
  }

};

namespace BI {
  namespace adl_helper {
    using std::to_string;

    template<class T>
    std::string as_string( T&& t ) {
      return to_string( std::forward<T>(t) );
    }
  }
  template<class T>
  std::string to_string( T&& t ) {
    return adl_helper::as_string(std::forward<T>(t));
  }
}

struct AssertionError : public std::exception {
  std::string message;
  ~AssertionError() throw () {}
  AssertionError(std::string msg) : message(msg) {}
  const char* what() const throw() { return message.c_str(); }
};

template <typename A, typename B>
void Assert(A a, B b) {
  if (a != b)
    throw AssertionError(BI::to_string(a) + " != " + BI::to_string(b));
}

template <typename A, typename B>
void AssertGreater(A a, B b) {
  if (!(a > b))
    throw AssertionError(BI::to_string(a) + " not greater than " + BI::to_string(b));
}

template <typename A, typename B>
void AssertLess(A a, B b) {
  if (!(a < b))
    throw AssertionError(BI::to_string(a) + " not less than " + BI::to_string(b));
}

#endif
