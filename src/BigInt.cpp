#include "BigInt.hpp"

BigInt ABS(BigInt b) {
  if (b < 0)
    return -b;
  else
    return b;
}

// Copy constructor
BigInt::BigInt(const BigInt& b) {
  num      = b.num;
  negative = b.negative;
}

// Assigns n to string n.
BigInt::BigInt(const int64_t n) {
  if (n < 0) {
    negative = true;
    num = std::to_string(std::abs(n));
  } else
    num = std::to_string(n);
}

// Loops over all characters in string and
// individually checks for it being numeric.
// If not, invalid_argument is thrown.
BigInt::BigInt(const char* s) {

  bool neg = s[0] == '-';

  if (neg)
    negative = true;

  for (unsigned long i = ( neg ? 1 : 0 ); i < strlen(s); ++i) {
    if (isdigit(s[i]))
      num += s[i];
    else
      throw std::invalid_argument("Invalid number provided.");
  }
}

// Arithmetic operators

// Applies a simple addition algorithm to strings
// to this->num + b.num and returns a BigInt instance.
BigInt BigInt::operator + (const BigInt& b) {

  // Check for negatives.

  // If left and right side have
  // different negative sign properties.
  if (this->negative != b.negative) {
    // If left side is negative
    // and greater than the other =>
    //     -b1 + b2 ; abs(b1) > b2
    //   ==> -(b1 - b2)
    if (this->negative && ABS(*this) > b) {
      return -(ABS(*this) - b);
    }
    // If left side is negative
    // and smaller than the other =>
    //     -b1 + b2 ; abs(b1) < b2
    //   ==> (b2 - b1)
    else if (this->negative && ABS(*this) < b) {
      return BigInt(b) - (*this).abs();
    }
    // If right side is negative
    // and smaller than the other =>
    //     b1 + -b2 ; b1 > abs(b2)
    //  ==> (b1 - b2)
    else if (b.negative && ABS(b) < *this) {
      return *this - ABS(b);
    }
    // If right side is negative
    // and greater than the other =>
    //     b1 + -b2 ; b1 < abs(b2)
    //  ==> -(b2 - b1)
    else if (b.negative && ABS(b) > *this) {
      return -(ABS(b) - *this);
    }
  }

  std::string s1 = num, s2 = b.num;

  if (s1.length() > s2.length())
    std::swap(s1, s2);

  // Stores resultant number.
  std::string res;

  auto n1 = s1.length(), n2 = s2.length();

  // Filler numbers.
  unsigned carry = 0;
  unsigned diff = n2 - n1;

  for (long long i = n1 - 1; i >= 0; --i) {
    auto sum = (s1[i] - '0') + (s2[i + diff] - '0') + carry;
    res.push_back(sum % 10 + '0');
    carry = sum / 10;
  }

  for (long long i = n2 - n1 - 1; i >= 0; --i) {
    auto sum = (s2[i] - '0') + carry;
    res.push_back(sum % 10 + '0');
    carry = sum / 10;
  }

  if (carry) res.push_back(carry + '0');

  reverse(res.begin(), res.end());

  // Convert to BigInt
  BigInt r(res);

  // Check if sign is negative. At this point
  // we've established that both BigInts are
  // negatives since different signs are checked
  // for in the beginning.
  if (this->negative) {
    return r.turn_negative();
  }

  return r;

}

// Simple subtraction algorithm.
BigInt BigInt::operator - (const BigInt& b) {

  bool neg = false;
  auto s1 = (*this).num, s2 = b.num;

  if (*this < b) {
    neg = true;
    std::swap(s1, s2);
  }

  // For storing result.
  std::string res;

  auto n1 = s1.length(), n2 = s2.length();
  auto diff = n1 - n2;

  int carry = 0;

  for (int i = n2 - 1; i >= 0; --i) {
    int sub = ((s1[i+diff] - '0') - (s2[i] - '0') - carry);
    if (sub < 0) {
      sub = sub + 10;
      carry = 1;
    } else
      carry = 0;
    res.push_back(sub + '0');
  }

  for (int i = n1 - n2 - 1; i >= 0; --i) {
    if (s1[i] == '0' && carry) {
      res.push_back('9');
      continue;
    }
    int sub = ((s1[i] - '0') - carry);
    if (i > 0 || sub > 0)
      res.push_back(sub + '0');
    carry = 0;
  }

  std::reverse(res.begin(), res.end());

  BigInt r(res);

  return (neg ? r.turn_negative() : r);

}

// Friend operator for right addition.

BigInt operator + (const int64_t n, BigInt& b) {
  return b + n;
}

BigInt operator - (const int64_t n, BigInt& b) {
  return BigInt(n) - b;
}

// Unary operators

BigInt BigInt::operator - () {
  if (*this < 0)
    return BigInt(this->num);
  else
    return BigInt("-" + this->num);
}

BigInt& BigInt::operator ++ () {
  *this += 1;
  return *this;
}

BigInt& BigInt::operator --() {
  *this -= 1;
  return *this;
}

// Comparision and assignment operators.

bool BigInt::operator == (const BigInt& b) {

  if (num.length() != b.num.length() || this->negative != b.negative){
    return false;
  }

  for (long unsigned int i = 0; i < num.length(); ++i)
    if (num[i] != b.num[i])
      return false;

  return true;

}

BigInt& BigInt::operator += (const BigInt& b) {
  *this = *this + 1;
  return *this;
}

BigInt& BigInt::operator -= (const BigInt& b) {
  *this = *this - 1;
  return *this;
}

BigInt BigInt::operator ++ (int) {
  BigInt r = *this;
  this->operator++();
  return r;
}

BigInt BigInt::operator -- (int) {
  BigInt r = *this;
  this->operator--();
  return r;
}

bool BigInt::operator < (const BigInt& b) {

  // Check for negatives.
  if (this->negative == b.negative && this->negative) {
      return (ABS(*this) > ABS(b));
  } else if (this->negative != b.negative) {
    return this->negative && !b.negative;
  }

  auto n1 = num.length(), n2 = b.num.length();

  if (n1 < n2)
    return true;
  else if (n2 < n1)
    return false;
  else {
    for (uint64_t i = 0; i < n1; ++i) {
      if (num[i] < b.num[i])
        return true;
      else if (num[i] > b.num[i])
        return false;
    }
    return false;
  }

}

bool BigInt::operator > (const BigInt& b) {

  // Check for negatives.
  if (this->negative == b.negative && this->negative) {
      return (ABS(*this) < ABS(b));
  } else if (this->negative != b.negative) {
    return !(this->negative && !b.negative);
  }

  auto n1 = num.length(), n2 = b.num.length();

  if (n1 > n2)
    return true;
  else if (n2 > n1)
    return false;
  else {
    for (uint64_t i = 0; i < n1; ++i) {
      if (num[i] > b.num[i])
        return true;
      else if (num[i] < b.num[i])
        return false;
    }
    return false;
  }
}

bool BigInt::operator <= (const BigInt& b) {
  return *this < b || *this == b;
}

bool BigInt::operator >= (const BigInt& b) {
  return *this > b || *this == b;
}

bool BigInt::operator != (const BigInt& b) {
  return !(*this == b);
}

// Boolean comparision operators for
// primitive numeric data types.

bool BigInt::operator == (const int64_t n) {
  return *this == BigInt(n);
}

bool BigInt::operator != (const int64_t n) {
  return *this != BigInt(n);
}

bool BigInt::operator < (const int64_t n) {
  return *this < BigInt(n);
}

bool BigInt::operator > (const int64_t n) {
  return *this > BigInt(n);
}

bool BigInt::operator <= (const int64_t n) {
  BigInt bn(n);
  return *this < bn || *this == bn;
}

bool BigInt::operator >= (const int64_t n) {
  BigInt bn(n);
  return *this > bn || *this == bn;
}

// IO

// Outputs string num when called from std::cout.
std::ostream& operator << (std::ostream& os, const BigInt& b) {
  if (b.negative)
    os << "-" << b.num;
  else
    os << b.num;
  return os;
}

// Inputs string (to store in num) from std::cin.
std::istream& operator >> (std::istream& in, BigInt& b) {
  std::cin >> b.num;
  return in;
}
