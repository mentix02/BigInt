# BigInt

![GitHub](https://img.shields.io/github/license/mentix02/BigInt)
![pipeline status](https://gitlab.com/mentix02/BigInt/badges/master/pipeline.svg)
![GitHub Workflow Status](https://img.shields.io/github/workflow/status/mentix02/BigInt/CppMakeAndTest)

An easy to use big integer library written in C++14. The BigInt class is arithmetically compatible with 
`ints`, `floats`, `longs` and other numeric data types. `BigInt`s can be initialized with strings as values
or numbers less than or equal to `INT64_MAX`.
