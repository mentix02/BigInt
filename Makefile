CC = clang++
CFLAGS = --std=c++14 -Wall -I include

dbg:
	${CC} ${CFLAGS} -g -c src/*
	mv *.o build
	${CC} ${CFLAGS} -o debug/BigInt main.cpp -g build/*

test:
	@printf "Building... "
	@${CC} ${CFLAGS} -g -c src/*
	@mv *.o build
	@${CC} ${CFLAGS} -o debug/BigInt main.cpp -g build/*
	@echo "\033[1;32mdone\033[0m.\n"
	@./debug/BigInt -v

bld:
	${CC} ${CFLAGS} -c src/*
	mv *.o build
	${CC} ${CFLAGS} -o bin/BigInt main.cpp build/*

clean:
	rm -rf bin/* debug/* build/*
	touch bin/.gitkeep debug/.gitkeep build/.gitkeep
