#include <vector>
#include <unordered_map>

#include "BigInt.hpp"

std::string red_text(std::string s) {
  std::string res = "\033[1;31m" + s + "\033[0m";
  return res;
}

std::string green_text(std::string s) {
  std::string res = "\033[1;32m" + s + "\033[0m";
  return res;
}

void TestPositiveConstructors() {

  BigInt b1(415);
  BigInt b2(INT64_MAX);
  BigInt b3("15821487138764826724867248624");

  Assert(b1, 415);
  Assert(b2, INT64_MAX);
  Assert(b3, BigInt("15821487138764826724867248624"));

}

void TestAssignmentConstructors() {

  BigInt b1 = 3415;
  BigInt b2 = -941;

  Assert(b1, 3415);
  Assert(b2, -941);

}

void TestNegativeConstructors() {

  BigInt b1(-415);
  BigInt b2(INT64_MIN);
  BigInt b3("-15821487138764826724867248624");

  Assert(b1, -415);
  Assert(b2, INT64_MIN);
  Assert(b3, BigInt("-15821487138764826724867248624"));

}

void TestSmallAddition() {

  BigInt b1("4156"), b2("5782784");
  BigInt b3("3419"), b4("9481947");
  BigInt b5("-295"), b6("-925820");

  Assert(b1 + b2, 4156 + 5782784);
  Assert(b3 + b4, 3419 + 9481947);
  Assert(b1 + b6, 4156 + -925820);
  Assert(b5 + b6, -925820 + -295);
  Assert(b2 + b5, 5782784 + -295);

}

void TestSmallSubtraction() {

  BigInt b1("5782784"), b2("4156");
  BigInt b3("9481947"), b4("3419");

  Assert(b1 - b2, 5782784 - 4156);
  Assert(b3 - b4, 9481947 - 3419);

}

void TestNegativeUnaryOperator() {

  BigInt b1(42624);
  BigInt b2("524248618574286246");

  Assert(-b1, -42624);
  Assert(-b2, BigInt("-524248618574286246"));

}

void TestComparisionOperators() {

  BigInt b1(842952), b2(-39414);
  BigInt b3("301333424941968934583"), b4("284752846924682476024572948572");

  AssertLess(b2, b1);
  AssertGreater(b1, b2);

}

void TestIncrementUnaryOperators() {

  BigInt b1(1345694);
  BigInt b2("652441449441405294");

  Assert(++b1, BigInt(1345695));
  Assert(b1, BigInt(1345695));
  Assert(++b2, BigInt("652441449441405295"));

  Assert(b1++, BigInt(1345695));
  Assert(b1, BigInt(1345696));

}

void TestDecrementUnaryOperators() {

  BigInt b1(1345694);
  BigInt b2("652441449441405294");

  Assert(--b1, BigInt(1345693));
  Assert(b1, BigInt(1345693));
  Assert(--b2, BigInt("652441449441405293"));

  Assert(b1--, BigInt(1345693));
  Assert(b1, BigInt(1345692));

}

// Generic function for running unit tests
// with different return types and arguments.
template <typename R, typename... Args>
void run(
  std::unordered_map<std::string, std::function<R(Args...)>>& tests,
  bool verbose = false) {

  bool failures = false;
  std::vector<std::string> errors(tests.size());

  for (const auto& test : tests) {

    bool failed = true;

    if (verbose)
      std::cout << "Running " << test.first << "... ";

    try {

      test.second();
      failed = false;

    } catch (const AssertionError& e) {

      failures = true;
      errors.push_back(red_text("FAILURE") + " : " + test.first + " ==> " + e.message);
      std::cout << (verbose ? (red_text("failed") + ".") : "F");
      std::cout << (verbose ? "\n" : "");

    }

    if (!failed)
      std::cout << (verbose ? (green_text("done") + ".\n") : ".");

  }

  if (failures) {
    std::cout << "\n-------------------------------------------\n\n";
    int count = 0;
    for (auto error : errors)
      if (error.length() != 0)
        std::cout << " # " << ++count << " : " << error << std::endl << std::endl;
    exit(-1);
  } else
    std::cout << (verbose ? "" : "\n");

}

int main(int argc, const char* argv[]) {

  bool verbose = false;

  if ( argc != 1 ) {
    for ( int i = 1; i < argc; ++i ) {
      if ( strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0 )
        verbose = true;
    }

  }

  std::unordered_map<std::string, std::function<void(void)>> tests = {
    {"TestSmallAddition", TestSmallAddition},
    {"TestSmallSubtraction", TestSmallSubtraction},
    {"TestComparisionOperators", TestComparisionOperators},
    {"TestPositiveConstructors", TestPositiveConstructors},
    {"TestNegativeConstructors", TestNegativeConstructors},
    {"TestNegativeUnaryOperator", TestNegativeUnaryOperator},
    {"TestAssignmentConstructors", TestAssignmentConstructors},
    {"TestDecrementUnaryOperators", TestDecrementUnaryOperators},
    {"TestIncrementUnaryOperators", TestIncrementUnaryOperators},
  };

  run(tests, verbose);

  return 0;

}
